package fibonacci;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Fibonacci {

	/**
	 * Given an array of ints, this method returns a sorted List that contains
	 * the Fibonacci numbers in the input list. The list returned contains the
	 * Fibonacci numbers in ascending order and has no duplicates.
	 * 
	 * @param inputArray
	 * @return a list of all the Fibonacci numbers in inputArray, in ascending
	 *         order, with no duplicates. If the input array is null or does not
	 *         contain any Fibonacci numbers then an empty list is returned.
	 */
	public static List<Integer> getFibonacciNumbers_sorted(Integer[] inputArray) {
		// TODO: Implement this method
		
		List<Integer> fibList = new ArrayList<Integer>();
		boolean existsInList = false;
		
		if(inputArray == null){
			List<Integer> emptyList1 = new ArrayList<Integer>();
			return emptyList1;
		}
		
		
		// Iterate through inputArray and check to see if Fibonacci numbers exist.
		for (int i=0; i < inputArray.length; i++){
			List<Integer> fibSeq = new ArrayList<Integer>();
			// Creates the beginning of the Fib sequence.
			fibSeq.add(1);
			fibSeq.add(1);
			
			// Creates the Fib sequence up to and possibly including the current value.
			int k = 0;
			int j = 0;
			while (j < (inputArray[i] +1) ){
				j = fibSeq.get(k) + fibSeq.get(k+1);
				fibSeq.add(j);
				// Adds the found Fib number to the List of Fib numbers in Array.
				if (j == inputArray[i]){
					// Checks to see if Fib number already exists in List.
					for (int l = 0; l < fibList.size(); l++){
						if(j == fibList.get(l)){
							existsInList = true;
						}
					}
					if( existsInList == false)fibList.add(j);
				}
				j=0;
				k++;
			}
			
			existsInList = false;		
		}
		
		// Sort fibList
		Collections.sort(fibList);
		
		return fibList; 
	}

}
